# #DengueZero #

## Objetivos ##
Projeto desenvolvido com o intuito de combater os focos do mosquito aedes aegypti.
Simples e prático, entre no aplicativo e cadastre um foco do mosquito, ajude a si mesmo e ao próximo.

Um mosquito não á mais forte que uma nação.

## Integração com o Google Maps ##

Fácil cadastro do foco do mosquito, entre na pagina principal cadastre um endereço de algum foco e faça sua parte.
### Integrantes ###

* [Leonardo de Cássio](https://bitbucket.org/leocassioo/)
* [Pedro Paulo](https://bitbucket.org/pedro0743/)
* [Walisson Andrade](https://bitbucket.org/walissonandrade/)
* [Sergio Lins](https://bitbucket.org/SergioLins001/)

##  ##